/**
 * Created by Ildar on 07.12.2015.
 */

$(function () {
    var chatBoxElem = $("#chatBox");

    var box = chatBoxElem.chatbox({
        id: user,
        user: {key: "value"},
        title: "Project Chat",
        messageSent: function (id, user, msg) {
            addChatMessage(id, msg);
        }
    });
    chatBoxElem.chatbox("option", "boxManager").toggleContent();

    function addChatMessage(id, message) {
        $.ajax({
            type: 'POST',
            url: addChatMessageUrl,
            data: JSON.stringify({message: message}),
            success: function (data) {
                loadNewMessages();
            },
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", token);
            }
        });
    }

    var gettingMsgs = false;
    var lastMsgDate = null;

    var loadNewMessages = function () {
        if (gettingMsgs) {
            return;
        }

        gettingMsgs = true;
        $.ajax({
            type: "GET",
            url: getChatMessagesUrl,
            data: {'last_msg_date': lastMsgDate},
            success: function (data) {
                $.each(data.new_messages, function (idx, msg) {
                    chatBoxElem.chatbox("option", "boxManager")
                        .addMsg(msg.username, msg.text);
                });
                if (data.last_message_date) {
                    lastMsgDate = data.last_message_date;
                }
                gettingMsgs = false;
            }
        });
    };

    setInterval(loadNewMessages, 3000);
    loadNewMessages();
});