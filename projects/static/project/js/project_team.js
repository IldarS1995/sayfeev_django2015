/**
 * Created by Ildar on 15.12.2015.
 */

function excludeUser(participant_id) {
    var form = $("#excludeUserForm");
    form.find("#participant_id").val(participant_id);
    form.submit();
}

var appointmentChangeUrl;
var csrfToken;

function changeRole(url, role) {
    $.ajax({
        'url': url,
        'contentType': 'application/json',
        'type': 'POST',
        'data': JSON.stringify({'role': role}),
        success: function (data) {
            if (data.success === false) {
                alert(data.message);
            }
        },
        beforeSend: function (xhr) {
            xhr.setRequestHeader("X-CSRFToken", csrfToken);
        }
    });
}

function changeAppointment(participantId, input) {
    newAppo = $(input).val();
    $.ajax({
        'url': appointmentChangeUrl,
        'contentType': 'application/json',
        'type': 'POST',
        'data': JSON.stringify({'participantId': participantId, 'appointment': newAppo}),
        success: function (data) {
            if (data.success === false) {
                alert(data.message);
            }
        },
        beforeSend: function (xhr) {
            xhr.setRequestHeader("X-CSRFToken", csrfToken);
        }
    });
}