/**
 * Created by Ildar on 10.12.2015.
 */

$(function () {
    var work_start = $("#id_work_start");
    var work_end = $("#id_work_deadline");

    $("#id_name").prop('required', 'required');
    work_start.prop('required', 'required');
    work_end.prop('required', 'required');

    function checkWorkDatesOrder() {
        var startDate = new Date(work_start.val());
        var endDate = new Date(work_end.val());

        if (startDate > endDate) {
            $("#errSpan").css('visibility', 'visible');
        }
        else {
            $("#errSpan").css('visibility', 'hidden');
        }
    }

    work_start.change(checkWorkDatesOrder);
    work_end.change(checkWorkDatesOrder);
});