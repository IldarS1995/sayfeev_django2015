# coding=utf-8
from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class Project(models.Model):
    """ Проект
    """
    name = models.CharField(max_length=150)  # Название проекта
    description = models.TextField(max_length=1000, null=True, blank=True)  # Описание
    work_start = models.DateField()  # Дата начала работы над проектом
    work_deadline = models.DateField()  # Дата окончания работы над проектом

    team = models.ManyToManyField(User,
                                  through='ProjectParticipant')  # Команда, работающая над проектом - список юзеров

    def __unicode__(self):
        return "(id=%d, name=%s)" % (self.id, self.name)


class ProjectDashboardNotification(models.Model):
    """
    Уведомления о каком-либо действии с проектом на главной странице проекта
    """
    project = models.ForeignKey(Project)  # Проект, в котором произошло действие
    text = models.CharField(max_length=250)  # Текст уведомления
    event_date = models.DateTimeField()  # Дата, когда событие произошло
    event_author = models.ForeignKey(User)  # Кто совершил действие, отраженное в данном уведомлении

    def __unicode__(self):
        return "(project=%s, author=%s, date=%s)" % (
            self.project, self.event_author, self.event_date)


class ProjectChatMessage(models.Model):
    """
    Сообщение в чате проекта
    """
    project = models.ForeignKey(Project)
    author = models.ForeignKey(User)  # Автор сообщения
    text = models.CharField(max_length=200)  # Текст сообщения
    publish_date = models.DateTimeField()  # Дата публикации сообщения в чате

    def __unicode__(self):
        return "(project=%s, author=%s, text=(%s), publish_date=%s)" \
               % (self.project, self.author,
                  self.text if len(self.text.__str__()) < 50 else (self.text[:50] + "..."),
                  self.publish_date)


class ProjectLink(models.Model):
    """
    Ссылка на какой-то сторонний ресурс, связанный с проектом, например, исходный код приложения проекта
    """
    project = models.ForeignKey(Project)
    url = models.URLField()
    description = models.TextField()
    publisher = models.ForeignKey(User)  # Кто разместил ссылку
    publish_date = models.DateTimeField()  # Дата публикации ссылки

    def __unicode__(self):
        return "(project=%s, url=%s)" % (self.project, self.url)


class ProjectMilestone(models.Model):
    """
    Промежуточный этап разработки проекта
    """
    project = models.ForeignKey(Project)
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    due_date = models.DateField()  # К какому числу этап должен быть готов
    responsible_people = models.ManyToManyField(User)  # Кто ответственнен за достижение этапа

    def __unicode__(self):
        return "(project=%s, name=%s, due date=%s)" % (self.project, self.name, self.due_date)


role_choices = (  # Роль(привилегии) в проекте
             ("ADMIN", "Administrator"),
             ("USER", "Teammate"),
          )


class ProjectParticipant(models.Model):
    """
    Участник проекта
    """
    project = models.ForeignKey(Project)
    participant = models.ForeignKey(User)
    appointment = models.CharField(max_length=100)  # Официальная должность в проекте - например, Java developer
    role = models.CharField(max_length=15, choices=role_choices)

    def __unicode__(self):
        return "(project=%s, participant=%s, role=%s)" % (self.project, self.participant, self.role)
