# coding=utf-8
import json
from datetime import datetime
import time
from django import forms
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse, reverse_lazy
from django.db.models import Q, Max
from django.http import HttpResponseRedirect, HttpResponseNotFound, JsonResponse, Http404
from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views.generic import DetailView, CreateView, ListView, DeleteView, UpdateView, FormView
from projects import models
from projects.forms import ParticipantForm, MilestoneForm, LinkForm
from projects.models import Project, ProjectParticipant, ProjectMilestone, ProjectLink, ProjectDashboardNotification, \
    ProjectChatMessage
from user_notifications.models import UserNotification
from django.db import connection


def add_notification(project, event_author, message):
    creation_time = timezone.now()
    notif = ProjectDashboardNotification.objects.create(project=project, text=message,
                                                        event_date=creation_time, event_author=event_author)
    for teammate in project.team.all():
        UserNotification.objects.create(notification=notif, user=teammate, is_read=False)


class LoggedInMixin(object):
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(LoggedInMixin, self).dispatch(*args, **kwargs)


def exists_participant_admin(project, user):
    """ Check if the user is an administrator in this project"""
    return ProjectParticipant.objects.filter(project=project, participant=user, role='ADMIN').exists()


class ViewProject(LoggedInMixin, DetailView):
    model = Project
    template_name = "projects/view_project.html"
    context_object_name = "project"

    def get_context_data(self, **kwargs):
        context = super(ViewProject, self).get_context_data(**kwargs)
        context['notifications'] = ProjectDashboardNotification.objects \
            .filter(project=self.get_object()).order_by('-event_date')
        return context

    def get(self, request, *args, **kwargs):
        project = self.get_object()
        if not project.team.filter(username=request.user.username).exists():
            return render(request, 'projects/restricted.html', {})
        return super(ViewProject, self).get(self, request, *args, **kwargs)


class CreateProject(LoggedInMixin, CreateView):
    model = Project
    fields = ["name", "description", "work_start", "work_deadline"]
    template_name = 'projects/create_project.html'

    def get_form(self, form_class=None):
        form = super(CreateProject, self).get_form(form_class)
        form.fields['work_start'].widget = forms.TextInput(attrs={'type': 'date'})
        form.fields['work_deadline'].widget = forms.TextInput(attrs={'type': 'date'})
        form.fields['appointment'] = forms.CharField(label="Your appointment", initial='Worker')
        return form

    def form_valid(self, form):
        if form.cleaned_data['work_start'] > form.cleaned_data['work_deadline']:
            form.add_error("work_start", "Work start date can't be greater than the work end date.")
            return self.form_invalid(form)

        project = form.save()

        if 'appointment' in form.cleaned_data and form.cleaned_data['appointment'] != '':
            appointment = form.cleaned_data['appointment']
        else:
            appointment = 'Worker'
        ProjectParticipant.objects.create(participant=self.request.user, role='ADMIN',
                                          appointment=appointment, project=project)

        # Create notification that the project has been created
        add_notification(project, self.request.user, "The project has been created.")

        return HttpResponseRedirect(reverse('projects:view_project', args=(project.id,)))


class ChooseProject(LoggedInMixin, ListView):
    template_name = 'projects/choose_project.html'
    context_object_name = 'projects'

    def get_queryset(self):
        return ProjectParticipant.objects.filter(participant=self.request.user)


class ProjectTeam(LoggedInMixin, DetailView):
    model = Project
    template_name = "projects/project_team.html"
    context_object_name = "project"

    def get_context_data(self, **kwargs):
        context = super(ProjectTeam, self).get_context_data(**kwargs)
        user = self.request.user
        project = context['project']
        q1 = Q(participant=user)
        q2 = Q(project=project)
        participants = ProjectParticipant.objects.filter(q2)
        participant = participants.get(q1 & q2)
        context['participants'] = participants
        context['isAdmin'] = (participant.role == 'ADMIN')
        context['role_choices'] = models.role_choices
        if 'error_msg' in self.request.session:
            context['error_msg'] = self.request.session['error_msg']
            del self.request.session['error_msg']
        return context


class AddPerson(LoggedInMixin, FormView):
    form_class = ParticipantForm
    template_name = "projects/add_person.html"

    def get_initial(self):
        project_id = self.kwargs['project_id']
        return {'project': project_id}

    def get_form_kwargs(self):
        kwargs = super(AddPerson, self).get_form_kwargs()
        kwargs.update({
            'request': self.request
        })
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(AddPerson, self).get_context_data(**kwargs)
        project = get_object_or_404(Project, pk=self.get_initial()['project'])
        context['project'] = project
        return context

    def form_valid(self, form):
        project_id = form.cleaned_data['project']
        person_username = form.cleaned_data['participant']
        appointment = form.cleaned_data['appointment']
        role = form.cleaned_data['role']

        project = Project.objects.get(id=project_id)
        person = User.objects.get(username=person_username)
        ProjectParticipant.objects.create(project=project, participant=person,
                                          appointment=appointment, role=role)

        add_notification(project, self.request.user, "The project has got new teammate - %s." % person_username)

        return HttpResponseRedirect(reverse("projects:project_team", args=(project_id,)))


@login_required()
def delete_person_from_project(request, project_id):
    if request.method == "GET":
        return HttpResponseNotFound()

    def send_redirect(error_msg=None):
        request.session["error_msg"] = error_msg
        return HttpResponseRedirect(reverse('projects:project_team', args=(project_id,)))

    try:
        participant_id = request.POST['participant_id']
    except KeyError:  # No id parameter was given
        error_msg = "No participant ID parameter was given."
        return send_redirect(error_msg)

    try:
        participant = ProjectParticipant.objects.get(id=participant_id)
    except ProjectParticipant.DoesNotExist:  # Such participant object doesn't exist
        error_msg = "The participant is not found."
        return send_redirect(error_msg)

    # Check that user is not trying to delete himself from project
    if request.user == participant.participant:
        error_msg = "You can't exclude yourself."
        return send_redirect(error_msg)

    project = participant.project
    # Check that current user is administrator in this project
    if not exists_participant_admin(project, request.user):
        error_msg = "You're not the administrator of this project."
        return send_redirect(error_msg)

    participant.delete()

    add_notification(project, request.user, "User %s was removed from the team." % participant.participant.username)

    return send_redirect()


@login_required()
def change_user_role(request, participant_id):
    data = json.loads(request.body)
    result = {'success': True, 'message': None}

    try:
        participant = ProjectParticipant.objects.get(id=participant_id)
    except ProjectParticipant.DoesNotExist:  # No participant found
        result['success'] = False
        result['message'] = "Participant doesn't exist."
        return JsonResponse(result, status=400)

    # Check if user trying to change another user's role is administrator
    if not exists_participant_admin(participant.project, request.user):
        result['success'] = False
        result['message'] = "You're not authorized to do this operation."
        return JsonResponse(result, status=400)

    role = data['role']
    participant.role = role
    participant.save()

    return JsonResponse(result, status=200)


@login_required()
def change_user_appointment(request):
    if request.method == "GET":
        raise Http404()

    data = json.loads(request.body)
    result = {'success': True, 'message': None}
    try:
        participant_id = data['participantId']
        appointment = data['appointment']
    except KeyError:
        result['success'] = False
        result['message'] = 'Specify both participant ID and an appointment.'
        return JsonResponse(result, status=400)

    participant = get_object_or_404(ProjectParticipant, pk=participant_id)

    # Check if user trying to change another user's role is administrator
    if not exists_participant_admin(participant.project, request.user):
        result['success'] = False
        result['message'] = "You're not authorized to do this operation."
        return JsonResponse(result, status=400)

    participant.appointment = appointment
    participant.save()

    return JsonResponse(result, status=200)


class DeleteProject(LoggedInMixin, DeleteView):
    model = Project
    template_name = "projects/delete_project.html"
    success_url = reverse_lazy('projects:choose_project')

    def dispatch(self, *args, **kwargs):
        project = self.get_object()
        # Check if project is wanted to be deleted by its administrator
        if not exists_participant_admin(project, self.request.user):
            return HttpResponseRedirect(reverse('projects:choose_project'))
        return super(DeleteProject, self).dispatch(*args, **kwargs)


def get_usernames(request, project_id):
    term = request.GET['term']
    team_usernames = ProjectParticipant.objects.filter(project__id=project_id)\
        .values_list('participant__username')
    users = User.objects.filter(username__contains=term).exclude(username__in=team_usernames)[:10]
    result = [obj[0] for obj in users.values_list('username')]
    return JsonResponse(result, status=200, safe=False)


@login_required()
def add_chat_message(request, project_id):
    project = get_object_or_404(Project, pk=project_id)
    data = json.loads(request.body)
    message = data['message']

    ProjectChatMessage.objects.create(project=project, author=request.user, text=message,
                                      publish_date=datetime.now())
    return JsonResponse({'success': True})


@login_required()
def get_chat_messages(request, project_id):
    try:
        last_msg_date = datetime.fromtimestamp(float(request.GET['last_msg_date']))
    except (KeyError, ValueError) as e:
        last_msg_date = None

    new_messages = ProjectChatMessage.objects.filter(project__id=project_id)
    if last_msg_date:
        new_messages = new_messages.filter(publish_date__gt=last_msg_date)
    new_messages = new_messages.order_by('publish_date')

    new_messages_json = [{'username': msg[0], 'text': msg[1]} for msg in
                         new_messages.values_list('author__username', 'text')]

    last_message_date = new_messages.all().aggregate(Max("publish_date"))
    if last_message_date:
        last_message_date = last_message_date['publish_date__max']
    if last_message_date:
        epoch = datetime.utcfromtimestamp(0)
        last_message_date = (last_message_date.replace(tzinfo=None) - epoch).total_seconds()

    return JsonResponse({'new_messages': new_messages_json, 'last_message_date': last_message_date}, safe=False)


class ViewMilestones(LoggedInMixin, DetailView):
    model = Project
    template_name = "projects/view_milestones.html"
    context_object_name = "project"


class AddMilestone(LoggedInMixin, CreateView):
    form_class = MilestoneForm
    template_name = "projects/add_milestone.html"

    def get_success_url(self):
        return reverse('projects:view_milestones', args=(self.kwargs['pk'],))

    def get_initial(self):
        project_id = self.kwargs['pk']
        return {'project': project_id}

    def get_form_kwargs(self):
        kwargs = super(AddMilestone, self).get_form_kwargs()
        kwargs.update({
            'request': self.request,
            'pk': self.kwargs['pk'],
        })
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(AddMilestone, self).get_context_data(**kwargs)
        context['project'] = get_object_or_404(Project, pk=self.kwargs['pk'])
        return context

    def form_valid(self, form):
        resp = super(AddMilestone, self).form_valid(form)
        project = get_object_or_404(Project, pk=self.kwargs['pk'])
        add_notification(project, self.request.user, "New milestone has been added.")
        return resp


class ViewMilestone(LoggedInMixin, DetailView):
    model = ProjectMilestone
    template_name = "projects/view_milestone.html"
    context_object_name = "milestone"

    def get_context_data(self, **kwargs):
        context = super(ViewMilestone, self).get_context_data(**kwargs)
        user = self.request.user
        milestone = context['milestone']
        q1 = Q(participant=user)
        q2 = Q(project=milestone.project)
        participants = ProjectParticipant.objects.filter(q2)
        participant = participants.get(q1 & q2)
        context['isAdmin'] = (participant.role == 'ADMIN')
        context['project'] = milestone.project
        return context


class EditMilestone(LoggedInMixin, UpdateView):
    form_class = MilestoneForm
    template_name = "projects/edit_milestone.html"

    def get_success_url(self):
        return reverse('projects:view_milestone', args=(self.kwargs['pk'],))

    def get_object(self, queryset=None):
        return get_object_or_404(ProjectMilestone, pk=self.kwargs['pk'])

    def get_form_kwargs(self):
        kwargs = super(EditMilestone, self).get_form_kwargs()
        milestone = self.get_object()
        kwargs.update({
            'request': self.request,
            'pk': milestone.project.id,
        })
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(EditMilestone, self).get_context_data(**kwargs)
        context['milestone_id'] = self.kwargs['pk']
        return context


@login_required()
def delete_milestone(request, milestone_id):
    user = request.user
    milestone = get_object_or_404(ProjectMilestone, pk=milestone_id)
    project = milestone.project
    # Check if user is administrator of this project
    if not exists_participant_admin(project, user):
        return render(request, 'projects/not_admin.html', {})
    milestone.delete()

    add_notification(project, request.user, "Milestone #%s \"%s\" has been deleted." % (milestone_id, milestone.name))

    return HttpResponseRedirect(reverse('projects:view_milestones', args=(project.id,)))


class ViewLinks(LoggedInMixin, DetailView):
    model = Project
    template_name = "projects/view_links.html"
    context_object_name = "project"

    def get_context_data(self, **kwargs):
        context = super(ViewLinks, self).get_context_data(**kwargs)
        user = self.request.user
        project = context['project']
        q1 = Q(participant=user)
        q2 = Q(project=project)
        participants = ProjectParticipant.objects.filter(q2)
        participant = participants.get(q1 & q2)
        context['isAdmin'] = (participant.role == 'ADMIN')
        return context


class CreateLink(LoggedInMixin, FormView):
    form_class = LinkForm
    template_name = "projects/add_link.html"

    def get_success_url(self):
        return reverse('projects:view_links', args=(self.kwargs['pk'],))

    def get_context_data(self, **kwargs):
        context = super(CreateLink, self).get_context_data(**kwargs)
        context['project_id'] = self.kwargs['pk']
        context['project'] = get_object_or_404(Project, pk=self.kwargs['pk'])
        return context

    def form_valid(self, form):
        url = form.cleaned_data['url']
        description = form.cleaned_data['description']
        project = get_object_or_404(Project, pk=self.kwargs['pk'])
        publisher = self.request.user
        publish_date = timezone.now()

        ProjectLink.objects.create(url=url, description=description, project=project,
                                   publish_date=publish_date, publisher=publisher)

        add_notification(project, self.request.user, "New link has been created.")

        return super(CreateLink, self).form_valid(form)


class EditLink(LoggedInMixin, FormView):
    form_class = LinkForm
    template_name = "projects/edit_link.html"

    def dispatch(self, *args, **kwargs):
        self.link = get_object_or_404(ProjectLink, id=self.kwargs['pk'])
        return super(EditLink, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(EditLink, self).get_context_data(**kwargs)
        context['link_id'] = self.kwargs['pk']
        return context

    def get_success_url(self):
        return reverse('projects:view_links', args=(self.link.project.id,))

    def get_initial(self):
        return {'url': self.link.url, 'description': self.link.description}

    def form_valid(self, form):
        self.link.url = form.cleaned_data['url']
        self.link.description = form.cleaned_data['description']
        self.link.save()
        return super(EditLink, self).form_valid(form)


@login_required()
def delete_link(request):
    link_id = request.POST['link_id']
    link = get_object_or_404(ProjectLink, pk=link_id)
    project = link.project
    # Check if user is admin in this project
    if not exists_participant_admin(project, request.user):
        return render(request, 'projects/not_admin.html', {})

    link.delete()

    add_notification(project, request.user, "The link #%s has been deleted." % link_id)

    return HttpResponseRedirect(reverse('projects:view_links', args=(project.id,)))


@login_required()
def view_statistics(request):
    cursor = connection.cursor()
    # Среднее количество тасков в ваших проектах
    raw_sql = '''SELECT avg(r.tasks_count)
                FROM
                  (SELECT
                     p.id,
                     count(t.id) AS tasks_count
                   FROM projects_project p
                     JOIN projects_projectparticipant pp
                       ON p.id = pp.project_id AND pp.role = 'ADMIN'
                          AND pp.participant_id = %d
                     LEFT JOIN tasks_task t
                       ON p.id = t.project_id
                   GROUP BY p.id) AS r;''' % request.user.id
    cursor.execute(raw_sql)
    row = cursor.fetchall()
    avg_val_in_your_projects = row[0]

    # Среднее количество тасков во всех проектах
    raw_sql = '''SELECT avg(r.tasks_count)
                FROM
                  (SELECT
                     p.id,
                     count(t.id) AS tasks_count
                   FROM projects_project p
                     LEFT JOIN tasks_task t
                       ON p.id = t.project_id
                   GROUP BY p.id) AS r;'''
    cursor.execute(raw_sql)
    row = cursor.fetchall()
    avg_val_in_all_projects = row[0]

    # Сколько проектов на сайте было начато в каждый из последних 12 месяцев
    raw_sql = '''SELECT
                  to_char(to_timestamp(date_part('month', p.work_start) :: TEXT, 'MMM'), 'Month') AS mon,
                  count(p.*)
                FROM public.projects_project AS p
                WHERE p.work_start > now() - INTERVAL '1 year'
                GROUP BY to_char(to_timestamp(date_part('month', p.work_start) :: TEXT, 'MMM'), 'Month');'''
    cursor.execute(raw_sql)
    row = cursor.fetchall()
    project_in_last_months = row

    # Самые популярные должности в ваших проектах
    raw_sql = '''SELECT
                  pp2.appointment,
                  count(pp2.appointment)
                FROM projects_projectparticipant pp
                  JOIN projects_project p
                    ON p.id = pp.project_id
                  JOIN projects_projectparticipant pp2
                    ON p.id = pp2.project_id
                WHERE pp.role = 'ADMIN' AND pp.participant_id = %d
                GROUP BY pp2.appointment
                ORDER BY count(pp2.appointment) DESC
                LIMIT 10;''' % request.user.id
    cursor.execute(raw_sql)
    row = cursor.fetchall()
    most_popular_appointments_in_your_projects = row

    # Самые популярные должности в проектах приложения
    raw_sql = '''SELECT
                  pp.appointment,
                  count(pp.appointment)
                FROM projects_projectparticipant pp
                  JOIN projects_project p
                    ON p.id = pp.project_id
                GROUP BY pp.appointment
                ORDER BY count(pp.appointment) DESC
                LIMIT 10;'''
    cursor.execute(raw_sql)
    row = cursor.fetchall()
    most_popular_appointment_in_app = row

    return render(request, 'projects/statistics.html', {
        'avg_val_in_your_projects': avg_val_in_your_projects,
        'avg_val_in_all_projects': avg_val_in_all_projects,
        'project_in_last_months': project_in_last_months,
        'most_popular_appointments_in_your_projects': most_popular_appointments_in_your_projects,
        'most_popular_appointment_in_app': most_popular_appointment_in_app
    });