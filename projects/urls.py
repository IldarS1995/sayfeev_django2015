# coding=utf-8
from django.conf.urls import url
from projects import views

__author__ = 'Ildar'

urlpatterns = [
    # Главная страница проекта
    url(r'(?P<pk>\d+)/view/$', views.ViewProject.as_view(), name='view_project'),
    # Создать новый проект
    url(r'create/$', views.CreateProject.as_view(), name='create_project'),
    # Выбрать проект из списка для начала работы с ним
    url(r'choose/$', views.ChooseProject.as_view(), name='choose_project'),
    # Посмотреть команду проекта
    url(r'(?P<pk>\d+)/team/$', views.ProjectTeam.as_view(), name='project_team'),
    # Добавить нового сотрудника в команду проекта
    url(r'(?P<project_id>\d+)/add_person/$', views.AddPerson.as_view(), name='add_person'),
    # Вернуть юзернеймы пользователей, которые подходят под поисковую фразу
    url(r'(?P<project_id>\d+)/get_usernames/$', views.get_usernames, name='get_usernames'),
    # Удалить пользователя из проекта
    url(r'(?P<project_id>\d+)/team/delete/$', views.delete_person_from_project,
        name='delete_person_from_project'),
    # Сменить роль пользователя в проекте, роль может менять только админ проекта
    url(r'team/(?P<participant_id>\d+)/change_role/$', views.change_user_role,
        name='change_user_role'),
    # Сменить должность пользователя в проекте, должность может менять только админ проекта
    url(r'team/change_user_appointment/$', views.change_user_appointment, name='change_user_appointment'),
    # Удалить проект
    url(r'(?P<pk>\d+)/delete/$', views.DeleteProject.as_view(), name='delete_project'),
    # Добавить сообщение в чат
    url(r'(?P<project_id>\d+)/add_chat_message/$', views.add_chat_message, name='add_chat_message'),
    # Получить новые сообщения чата
    url(r'(?P<project_id>\d+)/get_chat_messages/$', views.get_chat_messages,
        name='get_chat_messages'),
    # Посмотреть промежуточные этапы данного проекта
    url(r'(?P<pk>\d+)/milestones/$', views.ViewMilestones.as_view(), name='view_milestones'),
    # Добавить новый промежуточный этап проекту
    url(r'(?P<pk>\d+)/add_milestone/$', views.AddMilestone.as_view(), name='add_milestone'),
    # Посмотреть отдельный промежуточный этап этого проекта
    url(r'milestones/(?P<pk>\d+)/view_milestone/$', views.ViewMilestone.as_view(), name='view_milestone'),
    # Редактировать промежуточный этап
    url(r'milestones/(?P<pk>\d+)/edit/$', views.EditMilestone.as_view(), name='edit_milestone'),
    # Удалить промежуточный этап
    url(r'milestones/(?P<milestone_id>\d+)/delete_milestone/$', views.delete_milestone, name='delete_milestone'),
    # Посмотреть ссылки, созданные для проекта
    url(r'(?P<pk>\d+)/links/view/$', views.ViewLinks.as_view(), name='view_links'),
    # Создать новую ссылку для проекта
    url(r'(?P<pk>\d+)/links/create_link/$', views.CreateLink.as_view(), name='create_link'),
    # Редактировать существующую ссылку
    url(r'links/(?P<pk>\d+)/edit_link/$', views.EditLink.as_view(), name='edit_link'),
    # Удалить ссылку
    url(r'links/delete_link/$', views.delete_link, name='delete_link'),

    # Статистика по сайту
    url(r'statistics/$', views.view_statistics, name='view_statistics'),
]
