import json
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.test import TestCase
from django.utils import timezone
from projects.models import ProjectParticipant, Project


def login_user(client):
    username = 'ildar'
    password = 'ildar'
    user = User.objects.create_user(username=username, password=password)
    client.login(username=username, password=password)
    return user


def create_project():
    return Project.objects.create(name='Project', work_start=timezone.now(), work_deadline=timezone.now())


def create_pp(project, user, role='USER'):
    return ProjectParticipant.objects.create(project=project, participant=user,
                                               appointment='Hello', role=role)


def get_url():
    return reverse('projects:change_user_appointment')


class ChangeUserAppointment(TestCase):
    def test_method_is_get(self):
        login_user(self.client)
        response = self.client.get(get_url())
        self.assertEqual(response.status_code, 404)

    def test_participant_id_is_absent(self):
        login_user(self.client)
        response = self.client.post(get_url(), json.dumps({'appointment': 'Java Dev'}),
                                    content_type='application/json')
        self.assertEqual(response.status_code, 400)
        self.assertJSONEqual(response.content,
                             {'success': False,
                              'message': 'Specify both participant ID and an appointment.'})

    def test_participant_not_found(self):
        login_user(self.client)
        response = self.client.post(get_url(), json.dumps({'participantId': 10, 'appointment': 'Java Dev'}),
                                    content_type='application/json')
        self.assertEqual(response.status_code, 404)

    def test_this_user_is_not_project_admin(self):
        login_user(self.client)

        user = User.objects.create_user('someuser')

        project = create_project()
        pp = create_pp(project, user)

        response = self.client.post(get_url(), json.dumps({'participantId': pp.id, 'appointment': 'Java Dev'}),
                                    content_type='application/json')
        self.assertEqual(response.status_code, 400)
        self.assertJSONEqual(response.content,
                             {'success': False,
                              'message': "You're not authorized to do this operation."})

    def test_should_change_appointment(self):
        me = login_user(self.client)

        user = User.objects.create_user('someuser')

        project = create_project()
        pp = create_pp(project, user)
        create_pp(project, me, 'ADMIN')

        response = self.client.post(get_url(), json.dumps({'participantId': pp.id, 'appointment': 'Java Dev'}),
                                    content_type='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, {'success':True, 'message':None})

        pp = ProjectParticipant.objects.get(pk=pp.id)
        self.assertEqual(pp.appointment, 'Java Dev')