# coding=utf-8
from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db.models import Q
from django.forms import ModelForm, Form
from projects import models
from projects.models import ProjectParticipant, Project, ProjectMilestone

__author__ = 'Ildar'


class ParticipantForm(forms.Form):
    project = forms.IntegerField(widget=forms.HiddenInput())
    # participant - instead of drop down list with all people make dynamically
    # loading list of people that reacts on user typing
    participant = forms.CharField(max_length=30, label="Person's username")
    appointment = forms.CharField(max_length=100, label="Position in project")
    role = forms.CharField(widget=forms.Select(choices=models.role_choices))

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(ParticipantForm, self).__init__(*args, **kwargs)

    def clean_project(self):
        project_id = self.cleaned_data['project']
        try:
            project = Project.objects.get(id=project_id)
        except Project.DoesNotExist:
            raise ValidationError("Project doesn't exist")
        if not project.team.filter(username=self.request.user.username).exists():
            raise ValidationError("You have no relationship to this project.")
        q1 = Q(project=project)
        q2 = Q(participant=self.request.user)
        participant = ProjectParticipant.objects.get(q1 & q2)
        if not participant.role == 'ADMIN':
            raise ValidationError("You're not administrator of this project.")

        return project_id

    def clean_participant(self):
        participant_username = self.cleaned_data['participant']
        try:
            user = User.objects.get(username=participant_username)
        except User.DoesNotExist:
            raise ValidationError("The specified user doesn't exist.")

        project_id = self.cleaned_data['project']
        if ProjectParticipant.objects.filter(project__id=project_id, participant=user).exists():
            # Данный юзер уже является частью команды данного проекта
            raise ValidationError("This user is already in this project's team.")

        return participant_username


class MilestoneForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        pk = kwargs.pop('pk', None)
        super(MilestoneForm, self).__init__(*args, **kwargs)
        project = Project.objects.get(id=pk)
        self.fields['responsible_people'].queryset = project.team

    class Meta:
        model = ProjectMilestone
        fields = ["project", "name", "description", "due_date", "responsible_people"]
        widgets = {
            'project': forms.HiddenInput(),
            'responsible_people': forms.SelectMultiple(),
            'due_date' : forms.TextInput(attrs={'type':'date'}),
        }


class LinkForm(Form):
    url = forms.URLField()
    description = forms.CharField(widget=forms.Textarea)