from django.contrib import admin
from django.contrib.auth.models import User
from projects.models import *
from tasks.models import Task


class ParticipantInline(admin.TabularInline):
    model = ProjectParticipant
    extra = 1


class DashboardNotificationInline(admin.TabularInline):
    model = ProjectDashboardNotification
    extra = 1


class ChatMessageInline(admin.TabularInline):
    model = ProjectChatMessage
    extra = 1


class TaskInline(admin.TabularInline):
    model = Task
    extra = 1


class ProjectAdmin(admin.ModelAdmin):
    inlines = [ParticipantInline, DashboardNotificationInline, ChatMessageInline, TaskInline]
    list_display = ("name", "work_start", "work_deadline")
    list_filter = ["work_start", "work_deadline"]
    search_fields = ["name", "description"]

admin.site.register(Project, ProjectAdmin)


class ProjectParticipantAdmin(admin.ModelAdmin):
    list_display = ("project", "participant", "appointment", "role")
    list_filter = ["project__name", "role"]
    search_fields = ["appointment", "project__name", "participant__username"]

admin.site.register(ProjectParticipant, ProjectParticipantAdmin)


class ProjectLinkAdmin(admin.ModelAdmin):
    list_display = ("project", "url", "publisher", "publish_date")
    list_filter = ["project__name", "publish_date"]
    search_fields = ["project__name", "url", "publisher__username", "description"]

admin.site.register(ProjectLink, ProjectLinkAdmin)


class ProjectMilestoneAdmin(admin.ModelAdmin):
    list_display = ("project", "name", "due_date")
    list_filter = ["due_date", "project__name"]
    search_fields = ["project__name", "name", "description", "responsible_people__username"]

admin.site.register(ProjectMilestone, ProjectMilestoneAdmin)