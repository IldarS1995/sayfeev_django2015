from django.contrib import admin
from user_notifications.models import UserNotification


class UserNotificationAdmin(admin.ModelAdmin):
    list_display = ("notification", "user", "is_read")
    list_filter = ["user__username"]
    search_fields = ["notification", "user__username"]


admin.site.register(UserNotification, UserNotificationAdmin)