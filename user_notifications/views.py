from django.http import Http404, JsonResponse
from django.shortcuts import render

# Create your views here.
from user_notifications.models import UserNotification
from django.db import connection


def view_notifications(request):
    unread_notifications = UserNotification.objects.filter(user=request.user, is_read=False)\
        .order_by('-notification__event_date')
    read_notifications = UserNotification.objects.filter(user=request.user, is_read=True)\
        .order_by('-notification__event_date')

    return render(request, 'user_notifications/user_notifications.html',
                  {'unread_notifs': unread_notifications, 'read_notifs': read_notifications})


def notifications_read(request):
    if request.method == "GET":
        raise Http404()

    # Set these notifications as read. Next time user will not see them as new.
    raw_sql = "update user_notifications_usernotification " \
              "set is_read = true " \
              "where user_id = %d" % request.user.id
    connection.cursor().execute(raw_sql)

    return JsonResponse({'success': True})
