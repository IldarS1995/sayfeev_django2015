# coding=utf-8
from django.conf.urls import url
from user_notifications import views

__author__ = 'Ildar'

urlpatterns = [
    # Посмотреть новые уведомления пользователя по всем проектам
    url(r'notifications/$', views.view_notifications, name='view_notifications'),
    # Подтвердить, что новые уведомления прочитаны юзером
    url(r'notifications_read/$', views.notifications_read, name='notifications_read'),
]
