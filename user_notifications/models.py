# coding=utf-8
from django.contrib.auth.models import User
from django.db import models
from projects.models import ProjectDashboardNotification


class UserNotification(models.Model):
    """
    Уведомление юзера о событии в каком-либо проекте
    """
    notification = models.ForeignKey(ProjectDashboardNotification)
    user = models.ForeignKey(User)
    is_read = models.BooleanField()  # Прочитано ли уведомление юзером

    def __unicode__(self):
        return "(notification=%s, user=%s, is_read=%s)" % (self.notification, self.user, self.is_read)
