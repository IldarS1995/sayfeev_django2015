# coding=utf-8
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin

urlpatterns = [
    url(r'^$', 'users.views.start_page', name='start_page'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^users/', include('users.urls', namespace='users')),
    url(r'^projects/', include('projects.urls', namespace='projects')),
    url(r'^tasks/', include('tasks.urls', namespace='tasks')),
    url(r'^notifications/', include('user_notifications.urls', namespace='user_notifications')),
    # Вход в систему
    url(r'^login/$', 'django.contrib.auth.views.login', name='login'),
    # Операция выхода из системы
    url(r'^logout/$', 'django.contrib.auth.views.logout', name='logout'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
