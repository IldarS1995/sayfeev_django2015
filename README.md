# Project management application

## Description
This project is a tool that allows to create projects, add tasks to team members of a project, create milestones, links, etc.

## Domain
There are 11 entities in this project, including User.

* **User** - authenticated user of the system.

* **UserProfile** - additional details of user.

* **Project** - essential project data.

* **ProjectDashboardNotification** - information about some action that happened with this project. For example, some user has added new task, some user has edited a milestone, etc. This information is displayed on the project's main page called "Project Dashboard".

* **ProjectChatMessage** - every project has a chat, i.e. a collection of messages written by users that are part of the project's team.

* **ProjectLink** - URL to some resource connected to this project. This can be, for example, a source code repository.

* **ProjectMilestone** - represents a milestone of the project, i.e. some deadline, to which some part of the project must already be done.

* **ProjectParticipant** - member of a project's team. Each member of a project has some position (appointment, 'Worker' by default) in it and a role - administrator or plain teammate.

* **Task** - task pertaining to some project, assigned for execution to members of this project. Each task can have a supertask and subtasks, but not both at once. That is, a task can have one of the two states - "supertask", which doesn't have a supertask but can have subtasks, and "subtask", which can't have subtasks, but has a supertask.

* **TaskFile** - a file connected to some task's execution.

* **UserNotification** - when a new ProjectDashboardNotification is created, UserNotification is created for each member of this project's team and they can see this notification on the user's notifications page. When a user has seen new notifications, they are marked for him as read.

## Context
There are 3 layout contexts in this project:

* **Anon context** - when there's no authenticated user in the system. Anon can only access anon's main page, login and registration pages.

* **User context** - the user is authenticated, but he is outside the scope of any project. For example, this context is used when viewing all user's projects page.

* **Project context** - the user is viewing a page containing information about specific project. This can be, for example, project dashboard page, tasks page, milestones page etc.

## Roles
Every project has some team members. Each member has a role in this project. There are only two roles - Project administrator and Project teammate.

Project administrator can:

* Delete project;

* Add users to the project's team and remove them;

* Create, edit, delete tasks;

* Change role of any team member in the project except himself;

* Create, edit and delete milestones and links;

* Close any tasks;

* Change teammates' appointments in the project.

Project teammate can:

* Create milestones and links;

* Edit links;

* Close and reopen tasks assigned to him.

Both administrators and teammates can view project's dashboard, tasks, milestones, links, teammates and write to chat.