# coding=utf-8
from datetime import date
from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from projects.models import Project

task_choices = (  # Приоритет выполнения таска
            ("CRIT", "Critical"),
            ("HIGH", "High"),
            ("MED", "Medium"),
            ("LOW", "Low"),
            ("NONE", "None"),)


class Task(models.Model):
    """
    Таск в проекте
    """
    project = models.ForeignKey(Project)
    supertask = models.ForeignKey("self", null=True, blank=True)  # Главный таск. Этот таск является его подтаском.
    name = models.CharField(max_length=150)
    description = models.TextField(max_length=1000, blank=True)
    responsible_people = models.ManyToManyField(User)
    work_start = models.DateField(null=True, blank=True)  # Дата начала работы над таском
    work_end = models.DateField(null=True, blank=True)  # Дата окончания работы над таском
    priority = models.CharField(max_length=10, choices=task_choices)
    is_closed = models.BooleanField(default=False)

    @property
    def is_outdated(self):
        return (not self.is_closed) and (date.today() > self.work_end)

    def __unicode__(self):
        return "#%d: %s" % (self.id, self.name)


class TaskFile(models.Model):
    """
    Файл таска
    """
    task = models.ForeignKey(Task)
    add_date = models.DateTimeField()  # Дата добавления
    person_added = models.ForeignKey(User)  # Кем добавлен
    comment = models.TextField(max_length=1000, blank=True)
    file = models.FileField(upload_to="task_files")

    def __unicode__(self):
        return "(task=%s, file=%s)" % (self.task, self.file)
