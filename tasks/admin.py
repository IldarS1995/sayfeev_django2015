from django.contrib import admin
from tasks.models import Task, TaskFile


class SubtaskInline(admin.TabularInline):
    model = Task
    extra = 1


class TaskFileInline(admin.TabularInline):
    model = TaskFile
    extra = 1


class TaskAdmin(admin.ModelAdmin):
    inlines = [SubtaskInline, TaskFileInline]
    list_display = ("project", "supertask", "name", "work_start", "work_end")
    list_filter = ["work_start", "work_end", "project__name"]
    search_fields = ["project__name", "name", "description", "responsible_people__username"]


admin.site.register(Task, TaskAdmin)