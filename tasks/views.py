from datetime import datetime
import os
from django import forms
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse, reverse_lazy
from django.db.models import Q, F
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views.generic import DetailView, UpdateView
from projects.models import Project, ProjectParticipant, ProjectDashboardNotification
from tasks import models
from tasks.models import Task, TaskFile
from user_notifications.models import UserNotification


def add_notification(project, event_author, message):
    creation_time = timezone.now()
    notif = ProjectDashboardNotification.objects.create(project=project, text=message,
                                                        event_date=creation_time, event_author=event_author)
    for teammate in project.team.all():
        UserNotification.objects.create(notification=notif, user=teammate, is_read=False)


class LoggedInMixin(object):
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(LoggedInMixin, self).dispatch(*args, **kwargs)


def exists_participant_admin(project, user):
    """ Check if the user is an administrator of this project"""
    return ProjectParticipant.objects.filter(project=project, participant=user, role='ADMIN').exists()


def exists_participant(project, user):
    """ Check if the user is a participant of this project"""
    return ProjectParticipant.objects.filter(project=project, participant=user).exists()


@login_required()
def view_project_tasks(request, project_id):
    project = get_object_or_404(Project, id=project_id)
    if not exists_participant(project, request.user):
        raise Http404()

    # All tasks - show only primary tasks (that don't have supertasks);
    # Only administrators and tasks' responsible people can see closed tasks
    tasks = project.task_set.filter(supertask=None)
    isAdmin = exists_participant_admin(project, request.user)
    if not isAdmin:
        q1 = Q(is_closed=False)
        q2 = Q(is_closed=True)
        q3 = Q(responsible_people__in=[request.user])
        tasks = tasks.filter(q1 | q2 & q3).distinct()
    tasks = tasks.order_by('is_closed')

    # User's primary tasks
    my_tasks = tasks.filter(responsible_people__in=[request.user])

    # User's subtasks
    my_subtasks = project.task_set.filter(responsible_people__in=[request.user]).exclude(supertask=None)

    return render(request, 'tasks/view_project_tasks.html', {'project': project, 'tasks': tasks,
                                                             'my_tasks': my_tasks,
                                                             'my_subtasks': my_subtasks,
                                                             'isAdmin': isAdmin})


@login_required()
def add_task_to_project(request, project_id):
    project = get_object_or_404(Project, pk=project_id)
    # Check if user trying to add the task is this project's admin
    if not exists_participant_admin(project, request.user):
        return HttpResponseRedirect(reverse('projects:choose_project'))

    error_msg = None
    name = description = ''
    supertask_id = responsible_people = work_start = work_end = priority = None
    supertask = None
    if request.method == "POST":
        # Fields that can be null or empty
        if 'supertask' in request.POST and request.POST['supertask'] != '':
            supertask_id = request.POST['supertask']
        if 'description' in request.POST:
            description = request.POST['description']
        if 'work_start' in request.POST and request.POST['work_start'] != '':
            work_start = request.POST['work_start']
        if 'work_end' in request.POST and request.POST['work_end'] != '':
            work_end = request.POST['work_end']

        try:
            name = request.POST['name']
            responsible_people = request.POST.getlist('responsible_people')
            priority = request.POST['priority']
            if len(name) == 0 or len(responsible_people) == 0 or len(priority) == 0:
                raise KeyError
        except KeyError:
            error_msg = "Please fill all the required fields (name, responsible people, priority)."

        # Check if there's such a supertask if it was specified
        if error_msg is None and supertask_id is not None:
            try:
                supertask = Task.objects.get(id=supertask_id, project=project)
            except Task.DoesNotExist:
                error_msg = "The supertask doesn't exist."
        # Check if priority is in our list of priorities
        if error_msg is None:
            if not any(priority in choice for choice in models.task_choices):
                error_msg = "The priority is unknown."
        # Check if work_end is greater than work_start
        if error_msg is None and work_start is not None and work_end is not None:
            date_start = datetime.strptime(work_start, '%Y-%m-%d')
            date_end = datetime.strptime(work_end, '%Y-%m-%d')
            if date_start > date_end:
                error_msg = "Work start date can't be bigger than work end date."
        # Check if all people set as responsible exist and are part of this project's team
        if error_msg is None:
            responsible_people = [User.objects.get(username=username) for username in responsible_people]
            for person in responsible_people:
                if not ProjectParticipant.objects.filter(participant=person, project=project).exists():
                    error_msg = "Not all responsible people specified consist in this project's team."

        # Create new task
        if error_msg is None:
            task = Task.objects.create(supertask=supertask, name=name, description=description,
                                       project=project, work_start=work_start,
                                       work_end=work_end, priority=priority)
            add_notification(project, request.user, "Task #%d has been added to the project" % task.id)
            for teammate in responsible_people:
                task.responsible_people.add(teammate)
            return HttpResponseRedirect(reverse('tasks:view_project_tasks', args=(project_id,)))

    if supertask_id is not None:
        supertask_id = int(supertask_id)

    # Getting only primary tasks for choosing as a supertask
    tasks = project.task_set.filter(supertask=None, is_closed=False)

    return render(request, 'tasks/add_task_to_project.html',
                  {'project': project, 'tasks': tasks, 'priorities': models.task_choices,
                   'name': name, 'supertask': supertask_id, 'description': description,
                   'responsible_people': responsible_people, 'work_start': work_start,
                   'work_end': work_end, 'priority': priority, 'error_msg': error_msg})


class ViewTask(LoggedInMixin, DetailView):
    model = Task
    template_name = "tasks/view_task.html"
    context_object_name = "task"

    def dispatch(self, *args, **kwargs):
        task = self.get_object()
        # Check if the user trying to view the task is a member of this project's team
        if not exists_participant(task.project, self.request.user):
            raise Http404()
        # Restrict viewing closed tasks only to administrators and responsible people
        if task.is_closed:
            is_responsible = task.responsible_people.filter(username=self.request.user.username).exists()
            if not is_responsible and not exists_participant_admin(task.project, self.request.user):
                raise Http404()

        return super(ViewTask, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ViewTask, self).get_context_data(**kwargs)
        user = self.request.user
        task = self.get_object()
        # Set this value if this user is the administrator of this task's project
        context['isAdmin'] = exists_participant_admin(task.project, user)
        if 'error_msg' in self.request.session:
            context['error_msg'] = self.request.session['error_msg']
            del self.request.session['error_msg']
        context['project'] = task.project
        context['subtasks'] = task.task_set
        # If this is not an administrator, show only opened tasks
        if not exists_participant_admin(task.project, user):
            context['subtasks'] = context['subtasks'].filter(is_closed=False)
        context['subtasks'] = context['subtasks'].order_by('is_closed')
        context['isResponsible'] = task.responsible_people.filter(username=user.username).exists()
        return context


class EditTask(LoggedInMixin, UpdateView):
    model = Task
    fields = ['supertask', 'name', 'description', 'responsible_people',
              'work_start', 'work_end', 'priority', 'is_closed']
    template_name = "tasks/edit_task.html"

    def dispatch(self, *args, **kwargs):
        project = self.get_object().project
        if not exists_participant_admin(project, self.request.user):
            raise Http404()
        return super(EditTask, self).dispatch(*args, **kwargs)

    def get_success_url(self):
        return reverse('tasks:view_task', args=(self.kwargs['pk'],))

    def get_context_data(self, **kwargs):
        context = super(EditTask, self).get_context_data(**kwargs)
        context['project'] = self.get_object().project
        return context

    def form_valid(self, form):
        resp = super(EditTask, self).form_valid(form)
        add_notification(self.get_object().project, self.request.user,
                         "Task #%d has been modified." % self.get_object().id)
        return resp

    def get_form(self, form_class=None):
        form = super(EditTask, self).get_form(form_class)
        form.fields['work_start'].widget = forms.DateInput(attrs={'type': 'date'})
        form.fields['work_end'].widget = forms.DateInput(attrs={'type': 'date'})

        project = self.get_object().project
        # If this task has subtasks, restrict adding a supertask to it
        if self.get_object().task_set.count() != 0:
            form.fields['supertask'].queryset = Task.objects.none()
            form.fields['supertask'].widget = forms.HiddenInput()
        else:
            q1 = Q(supertask=None)  # Tasks without supertask
            q2 = Q(project=project)  # Tasks pertaining to this project
            q3 = Q(id=self.get_object().id)  # Tasks that aren't the task edited
            q4 = Q(is_closed=False)  # Tasks that aren't closed
            form.fields['supertask'].queryset = Task.objects.filter(q1 & q2 & ~q3 & q4)
        form.fields['responsible_people'].queryset = project.team

        return form


def close_task(request, task_id):
    if request.method == "GET":
        raise Http404()

    task = get_object_or_404(Task, pk=task_id)

    try:
        close_task = request.POST['close_task']
    except KeyError:
        return HttpResponseRedirect(reverse('tasks:view_project_tasks', args=(task.project.id,)))

    is_participant = task.responsible_people.filter(username=request.user.username).exists()
    # Check if user trying to close the task is an administrator or a responsible for this task person
    if not exists_participant_admin(task.project, request.user) and not is_participant:
        return HttpResponseRedirect(reverse('projects:choose_project'))

    task.is_closed = close_task == "1"
    task.save()

    add_notification(task.project, request.user,
                     "Task #%d \"%s\" has been %s." % (task.id, task.name,
                                                       "closed" if task.is_closed else "reopened"))

    return HttpResponseRedirect(reverse('tasks:view_project_tasks', args=(task.project.id,)))


def send_redirect_view_task(request, task_id, error_msg=None):
    request.session['error_msg'] = error_msg
    return HttpResponseRedirect(reverse('tasks:view_task', args=(task_id,)))


@login_required()
def delete_task(request, task_id):
    task = get_object_or_404(Task, pk=task_id)
    # Check if this user is the administrator of this task's project
    if not exists_participant_admin(task.project, request.user):
        return send_redirect_view_task(request, task_id, "You're not an administrator of this project.")

    if request.method == "GET":
        return render(request, 'tasks/delete_task.html', {'task': task})

    add_notification(task.project, request.user, "Task #%d \"%s\" has been deleted." % (task.id, task.name))

    project_id = task.project.id
    task.delete()

    return HttpResponseRedirect(reverse('tasks:view_project_tasks', args=(project_id,)))


@login_required()
def add_file_to_task(request, task_id):
    if request.method == "GET":
        raise Http404()

    task = get_object_or_404(Task, pk=task_id)
    # Check if this user belongs to this project. It not necessarily must be an administrator.
    if not exists_participant(task.project, request.user):
        return send_redirect_view_task(request, task_id, "You're not an administrator of this project.")

    if 'file' not in request.FILES:
        return send_redirect_view_task(request, task_id, "File for uploading must be specified.")

    comment = ''
    task_file = request.FILES['file']
    if 'comment' in request.POST:  # Comment can be empty
        comment = request.POST['comment']

    TaskFile.objects.create(task=task, add_date=datetime.now(), person_added=request.user,
                            comment=comment, file=task_file)

    add_notification(task.project, request.user, "New file has been added to the task #%d." % task.id)

    return send_redirect_view_task(request, task_id)


@login_required()
def download_file(request, file_id):
    task_file = get_object_or_404(TaskFile, pk=file_id)
    if not exists_participant(task_file.task.project, request.user):
        raise Http404()

    filename = os.path.basename(task_file.file.name)
    response = HttpResponse(task_file.file, content_type='text/plain')
    response['Content-Disposition'] = 'attachment; filename=%s' % filename

    return response


@login_required()
def remove_task_file(request, file_id):
    if request.method == "GET":
        raise Http404()

    task_file = get_object_or_404(TaskFile, pk=file_id)
    if not exists_participant_admin(task_file.task.project, request.user):
        return send_redirect_view_task(request, task_file.task.id, "You're not an administrator of this project.")

    task = task_file.task
    add_notification(task.project, request.user,
                     "File \"%s\" has been deleted from the task #%d." % (task_file.file.name, task.id))

    task_file.delete()

    return send_redirect_view_task(request, task_file.task.id)


@login_required()
def view_short_tasks(request, project_id):
    project = get_object_or_404(Project, pk=project_id)
    if not exists_participant(project, request.user):
        return HttpResponseRedirect(reverse('projects:choose_project'))

    tasks = Task.objects.filter(project=project, work_start__gte=F('work_end')-timezone.timedelta(weeks=1))

    return render(request, 'tasks/view_short_tasks.html', { 'project': project, 'tasks': tasks })