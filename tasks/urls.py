# coding=utf-8
from django.conf.urls import url
from tasks import views

__author__ = 'Ildar'

urlpatterns = [
    # Посмотреть все таски данного проекта
    url(r'view/(?P<project_id>\d+)/$', views.view_project_tasks, name='view_project_tasks'),
    # Добавить новый таск в проект
    url(r'add/(?P<project_id>\d+)/$', views.add_task_to_project, name='add_task_to_project'),
    # Посмотреть все "короткие" (меньше 7 дней) таски проекта
    url(r'/short_tasks/(?P<project_id>\d+)/$', views.view_short_tasks, name='view_short_tasks'),
    # Посмотреть детали отдельного таска
    url(r'(?P<pk>\d+)/view/$', views.ViewTask.as_view(), name='view_task'),
    # Редактировать таск
    url(r'(?P<pk>\d+)/edit/$', views.EditTask.as_view(), name='edit_task'),
    # Закрыть таск
    url(r'(?P<task_id>\d+)/close_task/$', views.close_task, name='close_task'),
    # Удалить таск
    url(r'(?P<task_id>\d+)/delete/$', views.delete_task, name='delete_task'),
    # Добавить файл таску
    url(r'(?P<task_id>\d+)/add_file/$', views.add_file_to_task, name='add_file_to_task'),
    # Загрузить файл
    url(r'download_file/(?P<file_id>\d+)/$', views.download_file, name='download_file'),
    # Удалить файл таска
    url(r'files/(?P<file_id>\d+)/remove/$', views.remove_task_file, name='remove_task_file'),
]
