from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.test import TestCase
from django.utils import timezone
from projects.models import Project, ProjectParticipant
from tasks.models import Task


def login_user(client):
    username = 'ildar'
    password = 'ildar'
    user = User.objects.create_user(username=username, password=password)
    client.login(username=username, password=password)
    return user


def create_project(name="Project"):
    return Project.objects.create(name=name, work_start=timezone.now(), work_deadline=timezone.now())


def create_project_participant(project, user, role):
    return ProjectParticipant.objects.create(project=project, participant=user, role=role, appointment='a')


def create_task(project, name, is_closed):
    return Task.objects.create(project=project, name=name, priority='CRIT', is_closed=is_closed)


class ViewProjectTasksTests(TestCase):
    def test_view_project_tasks_unauthorized(self):
        response = self.client.get(reverse('tasks:view_project_tasks', args=(1,)))
        self.assertEqual(response.status_code, 302)  # redirect to /login
        self.assertRedirects(response, reverse('login') +
                             '?next=' + reverse('tasks:view_project_tasks', args=(1,)))

    def test_view_project_tasks_project_not_found(self):
        login_user(self.client)
        response = self.client.get(reverse('tasks:view_project_tasks', args=(1,)))
        self.assertEqual(response.status_code, 404)

    def test_view_project_tasks_user_not_part_of_project(self):
        login_user(self.client)
        project = create_project()
        response = self.client.get(reverse('tasks:view_project_tasks', args=(project.id,)))
        self.assertEqual(response.status_code, 404)

    def test_view_project_tasks_user_admin(self):
        user = login_user(self.client)
        project = create_project('Project')
        create_project_participant(project, user, 'ADMIN')
        task1 = create_task(project, 'name 1', True)
        task2 = create_task(project, 'name 2', False)

        response = self.client.get(reverse('tasks:view_project_tasks', args=(project.id,)))
        self.assertContains(response, 'View project tasks', status_code=200)
        self.assertEqual(response.context['project'], project)
        self.assertEqual(len(response.context['tasks']), 2)
        self.assertTrue(task1 in response.context['tasks'])
        self.assertTrue(task2 in response.context['tasks'])

    def test_view_project_tasks_user_teammate(self):
        user = login_user(self.client)
        project = create_project('Project')
        create_project_participant(project, user, 'USER')
        task1 = create_task(project, 'name 1', True)
        task2 = create_task(project, 'name 2', False)

        # This task is closed, but pertains to this user; User must see it too
        task3 = create_task(project, 'name 3', True)
        task3.responsible_people.add(user)

        response = self.client.get(reverse('tasks:view_project_tasks', args=(project.id,)))
        self.assertContains(response, 'View project tasks', status_code=200)
        self.assertEqual(response.context['project'], project)
        self.assertEqual(len(response.context['tasks']), 2)
        self.assertTrue(task2 in response.context['tasks'])
        self.assertTrue(task3 in response.context['tasks'])
