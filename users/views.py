from django.contrib.auth import login, authenticate
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse_lazy, reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.utils.decorators import method_decorator
from users.forms import RegisterForm
from django.views.generic import CreateView, UpdateView, DetailView
from users.models import UserProfile


class RegistrationView(CreateView):
    form_class = RegisterForm
    template_name = 'registration/registration.html'

    def form_valid(self, form):
        user = form.save(commit=False)
        user.set_password(user.password)
        user.email = ''
        user.save()
        user = authenticate(username=self.request.POST['username'],
                            password=self.request.POST['password'])
        login(self.request, user)
        UserProfile.objects.create(user=user, email=self.request.POST['email'])
        return HttpResponseRedirect(reverse('projects:choose_project'))


class ProfileView(UpdateView):
    model = UserProfile
    template_name = "users/profile.html"
    context_object_name = "profile"
    fields = ["email", "firstName", "lastName", "speciality", "company", "country", "city", "avatar"]

    def form_valid(self, form):
        self.request.session['message'] = "Profile has been successfully updated."
        return super(ProfileView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(ProfileView, self).get_context_data(**kwargs)
        if 'message' in self.request.session:
            context['message'] = self.request.session['message']
            del self.request.session['message']
        return context

    def get_success_url(self):
        return reverse("users:profile")

    def get_object(self, queryset=None):
        user = self.request.user
        try:
            profile = UserProfile.objects.get(user=user)
        except UserProfile.DoesNotExist:
            profile = UserProfile.objects.create(user=user)
        return profile

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ProfileView, self).dispatch(*args, **kwargs)


class ViewUserProfile(DetailView):
    model = UserProfile
    template_name = "users/view_profile.html"
    context_object_name = "profile"

    def dispatch(self, request, *args, **kwargs):
        if request.user.id == int(self.kwargs['pk']):
            return HttpResponseRedirect(reverse('users:profile'))
        return super(ViewUserProfile, self).dispatch(request, *args, **kwargs)


def start_page(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('user_notifications:view_notifications'))

    return render(request, 'project/start_page.html', {})
