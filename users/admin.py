from django.contrib import admin
from users.models import UserProfile


class UserProfileAdmin(admin.ModelAdmin):
    list_display = ("user",)
    list_filter = ["speciality", "city"]
    search_fields = ["email", "firstName", "lastName", "country", "city", "speciality", "company"]

admin.site.register(UserProfile, UserProfileAdmin)