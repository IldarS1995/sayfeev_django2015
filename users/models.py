# coding=utf-8
from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class UserProfile(models.Model):
    """
    Профиль юзера
    """
    user = models.OneToOneField(User, primary_key=True)
    email = models.EmailField(unique=True)
    firstName = models.CharField(max_length=50, blank=True)
    lastName = models.CharField(max_length=50, blank=True)
    speciality = models.CharField(max_length=40, blank=True)  # Специальность юзера
    company = models.CharField(max_length=100, blank=True)  # Где работает
    country = models.CharField(max_length=100, blank=True)
    city = models.CharField(max_length=100, blank=True)
    avatar = models.ImageField(upload_to="user_images", null=True, blank=True)

    def __unicode__(self):
        return "(id=%s, user=%s, email=%s)" % (self.user_id, self.user, self.email)
