# coding=utf-8
from django.conf.urls import url
from users import views

__author__ = 'Ildar'

urlpatterns = [
    # Регистрация нового пользователя
    url('registration/$', views.RegistrationView.as_view(), name='registration'),

    # Профиль стороннего пользователя
    url('(?P<pk>\d+)/view_profile/$', views.ViewUserProfile.as_view(), name='view_profile'),

    # Профиль пользователя
    url('profile/$', views.ProfileView.as_view(), name='profile'),
]