from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.forms import ModelForm

__author__ = 'Ildar'


class RegisterForm(ModelForm):
    repeat_password = forms.CharField(widget=forms.PasswordInput)

    def clean(self):
        super(RegisterForm, self).clean()
        if 'password' in self._errors or 'repeat_password' in self._errors:
            return

        password = self.cleaned_data['password']
        repeat_password = self.cleaned_data['repeat_password']
        if password != repeat_password:
            self._errors['password'] = ['Passwords should be equal.']

    class Meta:
        model = User
        fields = ['username', 'email', 'password']
        widgets = {
            'password': forms.PasswordInput()
        }